This is a modified cgame(client game mod) for use on the Unfreeze servers the people at https://forum.fpsclassico.com/ are hosting.

# Usage

just drop zzzz-unfreeze-cgame.pk3 into the mod folder and you're done.

# Compilation

I used q3asm q3lcc and so on from ioquake3 and added them to the $PATH.
Then just execute the `compile` script.
